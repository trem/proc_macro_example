# Example Setup for Procedural Macros in Rust

This is a short tutorial for the setup necessary to create your own procedural macros in Rust.  
It does not go into depth on how to actually write the macro-code.

---

Procedural macros need to be defined in their own crate.  
You can, however, put this crate into a sub-directory in your repository, which is what we will do here.

To do so, run this from your repository root:
```bash
cargo init --lib my_custom_macro
```

This will create a sub-directory with own crate files, called `my_custom_macro`.  
The macro-crate needs to be specifically marked as a proc-macro crate.

For that, you need to add this to the `my_custom_macro/Cargo.toml`:
```toml
[lib]
proc-macro = true
```

Now you can define your procedural macros within `my_custom_macro/src/lib.rs`.  
Mind that procedural macros can only be defined in this lib.rs file (i.e. in the crate root).

You can for example add this code for an attribute-like procedural macro:
```rust
use proc_macro::TokenStream;

#[proc_macro_attribute]
pub fn debug(attr: TokenStream, item: TokenStream) -> TokenStream {
    println!("attr: \"{}\"", attr.to_string());
    println!("item: \"{}\"", item.to_string());
    item
}
```


To now use this macro in your project, you have to add the sub-directory as a dependency to your project.

Add this line to your project's Cargo.toml in the `[dependencies]` section:
```toml
my_custom_macro = { path = "my_custom_macro" }
```

Then use it in `src/main.rs` like so:
```rust
use my_custom_macro::debug;

#[debug]
fn example() {}
```

When compiling the project, this will now print out information about the TokenStreams.  
Mind that it won't do this, when there is nothing to compile, so when you're re-running it without code changes.
